import React from 'react'


export const IdProviderServices = {
    NOBankIdMobile:'urn:grn:authn:no:bankid:mobile',
    NOBankIdCentral:'urn:grn:authn:no:bankid:central',
    SEBankIDSameDevice:'urn:grn:authn:se:bankid:same-device',
    SEBankIDOtherDevice:'urn:grn:authn:se:bankid:another-device',
    DKNemIdPoces:'urn:grn:authn:dk:nemid:poces',
    DKNemIdMoces:'urn:grn:authn:dk:nemid:moces',
    DKNemIdCodeFile:'urn:grn:authn:dk:nemid:moces:codefile',
    NLDigIdBasic:'urn:grn:authn:nl:digid:basic',
    NLDigIdMiddle:'urn:grn:authn:nl:digid:middle',
    NLDigIdSubstantial:'urn:grn:authn:nl:digid:substantial',
    NLDigIdHigh:'urn:grn:authn:nl:digid:high'
}



export const BankIdConfig = {
    clientId : 'urn:my.client.id:12345',
    authority : 'https://my.authority.endpoint',
    redirectProtocol : window.location.protocol,
    redirectHostName : window.location.hostname,
    redirectPort : window.location.port,
    redirectPage : 'callback',
    idProviderService :  IdProviderServices.SEBankIDSameDevice,
}

export function BankId(props){
    /* eslint-disable */
    const authConfig = {
        client_id: props.clientId,
        redirect_uri: `${props.redirectProtocol}//${props.redirectHostName}${props.redirectPort ? `:${props.redirectPort}` : ''}/${props.redirectPage}`,
        response_type: 'id_token',
        scope: `openid${props.ssn?' sub:'+props.ssn:''}${props.phone?' phone:'+props.phone:''}`,
        authority: props.authority,
        automaticSilentRenew: false,
        acr_values:props.idProviderService,
        filterProtocolClaims: true,
    }
    /* eslint-enable */

    return(
        <a style={props.style} href={props.disabled?'javascript:;':authrequest(authConfig)} className={props.className}>{props.children}</a>
    )
}

function encodeData(data) {
    return Object.keys(data).map(function(key) {
        return [key, data[key]].map(encodeURIComponent).join('=')
    }).join('&')
}  

function authrequest(authConfig){
    const authUrl = `${authConfig.authority}/oauth2/authorize`
    return `${authUrl}?${encodeData(authConfig)}`
}

export default BankId
