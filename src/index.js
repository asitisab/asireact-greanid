import {BankId,BankIdConfig,IdProviderServices} from './BankId'

export {

  BankId,
  BankIdConfig,
  IdProviderServices
}
