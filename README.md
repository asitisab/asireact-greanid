# asireact-greanid

This react component from [Asitis AB](https://asitis.se) provides a convinient way to create links to https://grean.com/easyid endpoints for nordic bank id providers


*NOTE:* You need an account and a service agreement with Grean to use their service. Visit [grean.com](https://grean.com/easyid) to review their offer. The publisher of this component is in no way affiliated with Grean. 

---

## Installation
Execute one of the following commands in powershell
```shell
  # Yarn users
  yarn add asireact-greanid

  # NPM users
  npm install --save asireact-greanid
```

---

## Usage
To use the component, In your react Application import the BankId component, the configuration object and the service provider list.

```javascript

import {BankId,BankIdConfig,IdProviderServices } from 'asireact-greanid'

```

The following configurations must be provided:

```javascript
  let config = BankIdConfig;
  config.authority = 'https://mydomain.grean.id'; // The domain registered with Grean
  config.clientId = 'urn:my.client.id:3550'; // The client id provided by Grean when an application is registered
  config.idProviderService = IdProviderServices.SEBankIDSameDevice; // A provider service from the list
```

The following configurations are optional

```javascript
    config.redirectProtocol = "https"; // Defaults to current protocol
    config.redirectHostName = "my.service.host"; // Defaults to current host
    config.redirectPort = "1234"; // Defaults to current port (if used)
    config.redirectPage = 'mycallback';  // Defaults to 'callback'

    // This configuration will result in a callback with the id token from Grean to 
    // https://my.service.host:1234/mycallback 
```

The BankId component is used with the configuration like this 

```jsx
    <BankId {...config}>
      Logg in with bank id
    </BankId>
```

Or with an optional style object

```jsx
    <BankId {...config} style={{ background: this.props.color }}>
      Logg in with bank id
    </BankId>
```

Or with a regular className

```jsx
    <BankId {...config} className="btn">
      Logg in with bank id
    </BankId>
```

It is also possible to send along a SSN and/or a phone number to the component. This will be included in the scope of the request to Grean EasyId. This makes it possible to eliminate the form page asking for SSN on the provider site. The phone property is only useful when targeting Norwegian Mobile BankId.  

```jsx
    <BankId {...config} ssn="1234567890" phone='123456789' >
      Logg in with bank id
    </BankId>
```
