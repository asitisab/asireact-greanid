import React, { Component } from 'react'
import {BankId,BankIdConfig,IdProviderServices } from '../src/index'
import '../src/styles/style.scss'
import '../src/styles/bankid.scss'
import BIDLogo from '../src/images/bankid_vector_rgb.svg'


class Example extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''}
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  render() {

    const exampleStyle = {
      margin: 25,
      padding: 25,
      background: 'rgba(0,0,0,.05)',
      borderBottom: '2px solid rgba(0,0,0,.5)'
    }

    let config = BankIdConfig;
    config.authority = 'https://my.host.grean.id';
    config.clientId = 'urn:my.client.id:3550';
    config.idProviderService = IdProviderServices.SEBankIDSameDevice;


    return (
      <div>
        <div style={exampleStyle}>
          <h1>Example component</h1>
          <h2>I will help you test your components</h2>
          <em>You can locate me at ./devServer/Example.js</em>
          <h3>npm run release or yarn release will update version in package.json and publish to npm</h3>
        </div>
        <input text placeholder='Personnummer' onChange={this.handleChange}/>
        <BankId {...config} className='btn' phone={this.state.value} disabled={false}>Logga in med bankId
         <img src={BIDLogo} alt='npm logo' />
        </BankId>
      </div>
    )

  }
}

export default Example
